# -*- coding: utf-8 -*-
"""
Author:  Victor Faner

Description:  This module connects to a given ODBC database, executes a 
query, and saves the resulting table to a different database.
"""

import time
import logging

import pyodbc
import pandas as pd
from sqlalchemy import create_engine


def save_table(con_info, query, out_db, tbl_name, if_exists='append'):
    """
    Connect to ODBC, execute query, save to new db.
    
    :con_info:  str ODBC connection info
    :query:  str .sql or other text file containing query
    :out_db:  str output db address.  Must be in sqlalchemy-readable
              format
    :if_exists:  str out_db write action for pd.DataFrame.to_sql().    
                 Default 'append'.
    """
    # Connect to ODBC database
    con1 = pyodbc.connect(con_info)

    # Read .sql query file
    with open(query) as f:
        query = f.read()

    # Execute query
    start = time.time()
    logging.basicConfig(format='%(asctime)s: %(message)s')
    logging.warning('Executing query')
    df = pd.read_sql(query, con1)
    end = time.time()
    logging.warning('Query executed successfully. Elapsed time: %s seconds', 
                    end-start)
    con1.close()

    # Save to output database
    con2 = create_engine(out_db)        
    df.to_sql(tbl_name, con=con2, if_exists=if_exists)    


if __name__ == '__main__':
    import os
    
    QUERY = 'queries/test.sql'
    CON_INFO = ('DRIVER={ODBC Driver 13 for SQL Server};'
                'SERVER=TLXSQLPROD-01;'
                'DATABASE=ODS_CPS;'
                'Trusted_Connection=yes')
    OUT_DB = str('sqlite:///' + os.getcwd() + '\\test.db')
    TBL_NAME = 'Test_Table'
    
    save_table(CON_INFO, QUERY, OUT_DB, TBL_NAME)
